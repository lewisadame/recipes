<?php
    require_once '../setup.php';
    require_once '../database/connection.php';

    $errors = [];
    if (isset($_POST['login'])) {
        $username = $_POST['username'] ?? null;
        $password = $_POST['password'] ?? null;

        // Validaciones
        if (empty($username)) {
            $errors['username']['empty'] = "Debes introducir un nombre.";
            $username = null;
        }

        if (empty($password)) {
            $errors['password']['empty'] = "Debes facilitar una contraseña.";
        }

        if (empty($errors)) {
            $query = "SELECT * FROM users WHERE username = '$username' limit 1";
            $res = mysqli_query($db, $query);
            $data = mysqli_fetch_assoc($res);

            if (password_verify($password, $data['password'])) {
                $data['password'] = null;
                $_SESSION['user'] = $data;
                logger(
                    $db,
                    'logins',
                    $data['id'] ?? 'null',
                    1,
                    "'" . $_SERVER['REMOTE_ADDR'] . "'",
                    'NOW()'
                );
                redirect(BASE_URL);
            } else {
                logger(
                    $db,
                    'logins',
                    $data['id'] ?? 'null',
                    0,
                    "'" . $_SERVER['REMOTE_ADDR'] . "'",
                    'NOW()'
                );
                $errors['login'] = "Credentials do not match";
            }
        }
    }
    set_body_class(['login']);
    require 'login.view.php';
