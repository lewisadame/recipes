<?php
    require_once '../setup.php';
    require_once '../includes/header.php';
?>
<main class="container login">
    <div class="offset-3 col-6 pt-4 pb-4">
        <form action="" method="POST" novalidate>
        <div class="form-group">
                <?php if ($errors['login']): ?>
                    <div class="alert alert-danger" role="alert">
                        <?=$errors['login']?>
                    </div>
                <?php endif; ?>
                <label for="username">Username</label>
                <input type="text" class="form-control <?=($errors['username'])?"is-invalid":""?>" id="username" name="username" placeholder="Enter your username" value="<?=($username??'')?>">
                <?php errors($errors, 'username'); ?>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control <?=($errors['password'])?"is-invalid":""?>" id="password" name="password" aria-describedby="passwordHelp" placeholder="Password">
                <?php errors($errors, 'password'); ?>
            </div>
            <button type="submit" name="login" class="btn btn-primary">Log In</button>
            <span class="ml-1 mr-1">Don't you have an account?</span><a href="/signup" class="btn btn-warning">Signup!</a>
        </form>
    </div>
</main>
<?php require_once '../includes/footer.php'; ?>
