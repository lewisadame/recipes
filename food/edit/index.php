<?php
require_once '../../setup.php';
require_once '../../database/connection.php';
require_once '../../validation/Validate.php';

if (has_input('edit-food')) {
    $rules = [
        'food_id' => 'integer',
        'name' => 'string',
        'expDate' => 'date'
    ];

    $data = filter_input_array(INPUT_POST, [
        'food_id' => FILTER_SANITIZE_NUMBER_INT,
        'name' => FILTER_SANITIZE_STRING,
        'expDate' => FILTER_SANITIZE_STRING
    ]);

    $validator = new Validate($rules, $data);
    $validator->make();

    if ($validator->fails()) {
        $errors = $validator->messages();
        require_once 'edit-food.view.php';
    } else {
        $query = "update food set name = '{$data['name']}', expiration_date = '{$data['expDate']}' where id = '{$data['food_id']}'";

        $result = mysqli_query($db, $query);

        if ($result) {
            message('Your food was edited!', 'success');
            redirect(BASE_URL . '/food');
        } else {
            message('We had a problem editing your food...', 'danger');
            redirect(BASE_URL . '/food');
        }
    }
} else {
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

    if ($id) {
        $query = "select * from food where id='$id' limit 1";
        $result = mysqli_query($db, $query);
        $food = mysqli_fetch_assoc($result);
        if ($food) {
            require_once 'edit-food.view.php';
        } else {
            redirect(BASE_URL . '/food');
        }
    } else {
        redirect(BASE_URL . '/food');
    }
}
