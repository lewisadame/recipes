<?php
require_once '../../includes/header.php';
?>
<main class="container">
    <div class="offset-3 col-6 pt-4 pb-4">
        <form action="" method="POST"  novalidate>
            <input type="hidden" name="food_id" value="<?=$food['id']?>">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control <?= ($errors['name']) ? "is-invalid" : "" ?>" id="name" name="name" aria-describedby="nameHelp" placeholder="Enter a food name" value="<?= ($food['name'] ?? '') ?>">
                <small id="nameHelp" class="form-text text-muted">Write the title or name of your food</small>
                <?php errors($errors, 'name'); ?>
            </div>
            <?php if ($food['expiration_date']) {
    $date = new DateTime($food['expiration_date']);
} ?>
            <div class="form-group">
                <label for="expDate">Expiration date</label>
                <input type="date" class="form-control <?= ($errors['expDate']) ? "is-invalid" : "" ?>" id="expDate" name="expDate" aria-describedby="expDateHelp" placeholder="Enter the food's expiration date"  value="<?= (date_format($date, 'Y-m-d') ?? '') ?>">
                <?php errors($errors, 'expDate'); ?>
            </div>
            <button type="submit" name="edit-food" class="btn btn-primary">Edit food!</button>
        </form>
    </div>
</main>
<?php require_once '../../includes/footer.php'; ?>
