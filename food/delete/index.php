<?php
require_once '../../setup.php';
require_once '../../database/connection.php';
require_once '../../validation/Validate.php';

if (has_input('delete-food')) {
    $data = filter_input_array(INPUT_POST, [
        'food_id' => FILTER_SANITIZE_NUMBER_INT
    ]);
    $query = "select * from food where id='{$data['food_id']}' limit 1";
    $result = mysqli_query($db, $query);
    $recipe = mysqli_fetch_assoc($result);
    if (!$recipe) {
        redirect(BASE_URL . '/food');
        return;
    }

    $rules = [
        'food_id' => 'integer'
    ];

    $validator = new Validate($rules, $data);
    $validator->make();

    if ($validator->fails()) {
        redirect(BASE_URL . '/food');
    } else {
        $query = "delete from food where id = '{$data['food_id']}' limit 1";

        $result = mysqli_query($db, $query);

        if ($result) {
            message('Your food was deleted!', 'success');
            redirect(BASE_URL . '/food');
        } else {
            message('We had a problem deleting your food...', 'danger');
            redirect(BASE_URL . '/food');
        }
    }
} else {
    redirect(BASE_URL . '/food');
}
