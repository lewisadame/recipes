<?php
require_once '../../includes/header.php';
?>
<main class="container">
    <div class="offset-3 col-6 pt-4 pb-4">
        <form action="" method="POST"  novalidate>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control <?= ($errors['name']) ? "is-invalid" : "" ?>" id="name" name="name" aria-describedby="nameHelp" placeholder="Enter a food name" value="<?= ($name ?? '') ?>">
                <small id="nameHelp" class="form-text text-muted">Write the title or name of your food</small>
                <?php errors($errors, 'name'); ?>
            </div>
            <div class="form-group">
                <label for="expDate">Expiration date</label>
                <input type="date" class="form-control <?= ($errors['expDate']) ? "is-invalid" : "" ?>" id="expDate" name="expDate" aria-describedby="expDateHelp" placeholder="Enter the food's expiration date"  value="<?= ($expDate ?? '') ?>">
                <?php errors($errors, 'expDate'); ?>
            </div>
            <button type="submit" name="create-food" class="btn btn-primary">Create food!</button>
        </form>
    </div>
</main>
<?php require_once '../../includes/footer.php'; ?>
