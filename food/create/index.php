<?php

require_once '../../setup.php';
require_once '../../validation/Validate.php';
require_once BASE_PATH . '/database/connection.php';

if (has_input('create-food')) {
    $rules = [
        'name' => 'required|string',
        'expDate' => 'required|date'
    ];

    $data = filter_input_array(INPUT_POST, [
        'name' => FILTER_SANITIZE_STRING,
        'expDate' => FILTER_SANITIZE_STRING
    ]);

    $validator = new Validate($rules, $data);
    $validator->make();

    if ($validator->fails()) {
        require_once 'create-food.view.php';
    } else {
        $query = "INSERT INTO food (name, expiration_date) VALUES ('{$data['name']}', '{$data['expDate']}')";
        $result = mysqli_query($db, $query);

        if ($result) {
            message('Your food was saved!', 'success');
            redirect(BASE_URL . '/food');
        } else {
            message('We had a problem saving your food...', 'danger');
            redirect(BASE_URL . '/food');
        }
    }
} else {
    set_body_class([]);
    require_once 'create-food.view.php';
}
