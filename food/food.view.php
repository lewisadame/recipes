<?php
require_once '../setup.php';
require_once '../includes/header.php';
?>
<main class="container">
    <?=get_message();?>
    <?php flush_message(); ?>
    <h1>Food</h1>
    <div class="food">
        <?php if (isset($food) && count($food) > 0): foreach ($food as $item): ?>
        <div class="card item">
            <div class="card-body d-flex flex-column">
                <h5 class="card-title">
                    <?=$item['name'];?>
                </h5>
                <h6 class="card-subtitle mb-2 text-muted">
                    Expiration: <?=$item['expiration_date'];?>
                </h6>
                <div class="options">
                    <a href="/food/edit?id=<?=$item['id'];?>" class="btn btn-primary">Editar</a>
                    <form action="/food/delete" method="post">
                        <input type="hidden" name="food_id" value="<?=$item['id'];?>">
                        <button class="btn btn-link" name="delete-food">Delete</button>
                    </form>
                </div>
            </div>
        </div>
        <?php endforeach; else: ?>
        Oops no hay comida!
        <?php endif; ?>
    </div>
</main>
<?php require_once '../includes/footer.php'; ?>
