<?php

function redirect($url)
{
    header('Location: ' . $url);
}

function session($entries)
{
    $entries = explode('.', $entries);
    $result = $_SESSION;
    foreach ($entries as $entry) {
        $result = $result[$entry];
    }
    return $result;
}

function message($value, $type)
{
    $_SESSION['message'] = [
        'value' => $value,
        'type' => $type
    ];
}

function get_message()
{
    if (session('message')) {
        $markup = "<div class='alert alert-" . session('message.type') . " role='alert'>" . session('message.value') . "</div>";
        return $markup;
    }
}

function flush_message()
{
    if (session('message')) {
        unset($_SESSION['message']);
    }
}

function logger($link, $table, ...$values)
{
    $values = implode(', ', $values);
    $query = "INSERT INTO {$table} VALUES({$values})";
    mysqli_query($link, $query);
}

function errors($errors, $entry)
{
    if (!empty($errors[$entry])) {
        $markup = '<div class="invalid-feedback">';
        if (is_array($errors[$entry])) {
            foreach ($errors[$entry] as $error) {
                $markup .= "{$error}</br>";
            }
        } else {
            $markup .= "{$errors[$entry]}</br>";
        }
        $markup .= "</div>";
        echo $markup;
    }
}

function input($key, $type = INPUT_POST)
{
    return filter_input($type, $key, FILTER_SANITIZE_STRING);
}

function has_input($key, $type = INPUT_POST)
{
    return filter_has_var($type, $key);
}

function set_body_class($array = [])
{
    $_SESSION['body_class'] = $array;
}

function get_body_class()
{
    $classes = session('body_class');
    return $classes;
}

function body_class()
{
    $classes = get_body_class();
    if (is_array($classes)) {
        echo implode(' ', $classes);
    }
}


function excerpt($string, $max)
{
    $subs = substr($string, 0, $max);
    $lastChar = substr($subs, -1);
    if ($lastChar === "." || $lastChar === " ") {
        $subs = substr($subs, 0, strlen($subs) - 1) . "...";
    } else {
        $subs = $subs . "...";
    }
    return $subs;
}

function auth_check($password)
{
    global $db;
    $user_id = session('user.id');
    $query = "select password from users where id = '$user_id'";
    $result = mysqli_query($db, $query);
    $saved_password_hash = mysqli_fetch_assoc($result)['password'];
    return password_verify($password, $saved_password_hash);
}
