<?php
require_once '../../setup.php';
require_once '../../database/connection.php';
require_once '../../validation/Validate.php';

if (has_input('delete-food')) {
    $data = filter_input_array(INPUT_POST, [
        'food_id' => FILTER_SANITIZE_NUMBER_INT,
        'inventory_id' => FILTER_SANITIZE_NUMBER_INT
    ]);

    $query = "select * from inventories_food where food_id='{$data['food_id']}' and inventory_id = '{$data['inventory_id']}' limit 1";
    $result = mysqli_query($db, $query);
    $inventory = mysqli_fetch_assoc($result);
    if (!$inventory) {
        redirect(BASE_URL . '/inventory');
        return;
    }

    $rules = [
        'food_id' => 'integer',
        'inventory_id' => 'integer'
    ];

    $validator = new Validate($rules, $data);
    $validator->make();

    if ($validator->fails()) {
        redirect(BASE_URL . '/inventory');
    } else {
        $query = "delete from inventories_food where food_id='{$data['food_id']}' and inventory_id = '{$data['inventory_id']}' limit 1";

        $result = mysqli_query($db, $query);

        if ($result) {
            message('Your food was deleted!', 'success');
            redirect(BASE_URL . '/inventory');
        } else {
            message('We had a problem deleting your food...', 'danger');
            redirect(BASE_URL . '/inventory');
        }
    }
} else {
    redirect(BASE_URL . '/inventory');
}
