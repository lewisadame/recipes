<?php
require_once '../setup.php';
require_once '../database/connection.php';

// fetch inventory
$user_id = session('user.id');
$query = "select * from inventories where user_id = '$user_id'";
$result = mysqli_query($db, $query);
$inventory = mysqli_fetch_assoc($result);
$inventory_id = $inventory['id'];

// and fetch its content
$query = "select * from inventories_food i join food f on i.food_id = f.id where inventory_id = '$inventory_id'";
$result = mysqli_query($db, $query);
$food = mysqli_fetch_all($result, MYSQLI_ASSOC);

require_once 'inventory.view.php';
