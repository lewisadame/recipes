<?php
require_once '../../includes/header.php';
?>
<main class="container">
    <div class="offset-3 col-6 pt-4 pb-4">
        <form action="" method="POST"  novalidate>
            <div class="form-group">
                <label for="food">Food</label>
                <select name="food_id" id="food">
                    <?php foreach ($food as $item): ?>
                        <option value="<?=$item['id']?>">
                            <?=$item['name']?>
                        </option>
                    <?php endforeach; ?>
                </select>
                <?php errors($errors, 'food'); ?>
            </div>
            <button type="submit" name="add-food" class="btn btn-primary">Add food!</button>
        </form>
    </div>
</main>
<?php require_once '../../includes/footer.php'; ?>
