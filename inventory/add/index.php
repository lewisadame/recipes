<?php

require_once '../../setup.php';
require_once '../../validation/Validate.php';
require_once BASE_PATH . '/database/connection.php';

if (has_input('add-food')) {
    $rules = [
        'food_id' => 'required|integer'
    ];

    $data = filter_input_array(INPUT_POST, [
        'food_id' => FILTER_SANITIZE_NUMBER_INT,
    ]);

    $validator = new Validate($rules, $data);
    $validator->make();

    if ($validator->fails()) {
        require_once 'add-food.view.php';
    } else {
        //fetch inventory1
        $user_id = session('user.id');
        $query = "select * from inventories where user_id = '$user_id' limit 1";
        $result = mysqli_query($db, $query);
        $inventory_id = mysqli_fetch_assoc($result)['id'];
        $query = "INSERT INTO inventories_food (inventory_id, food_id) VALUES ('$inventory_id', '{$data['food_id']}')";
        $result = mysqli_query($db, $query);

        if ($result) {
            message('Your food was saved!', 'success');
            redirect(BASE_URL . '/inventory');
        } else {
            message('We had a problem saving your food...', 'danger');
            redirect(BASE_URL . '/inventory');
        }
    }
} else {
    set_body_class([]);
    $query = "select * from food";
    $result = mysqli_query($db, $query);
    $food = mysqli_fetch_all($result, MYSQLI_ASSOC);
    require_once 'add-food.view.php';
}
