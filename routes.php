<?php

$routes = [
    'login' => ['middleware' => 'guest'],
    'signup' => ['middleware' => 'guest'],
    'logout' => ['middleware' => 'authenticated'],
    'recipes' => ['middleware' => 'authenticated'],
    'recipes/create' => ['middleware' => 'authenticated'],
    'recipes/edit' => ['middleware' => 'authenticated'],
    'recipes/delete' => ['middleware' => 'authenticated'],
    'food' => ['middleware' => 'authenticated'],
    'food/create' => ['middleware' => 'authenticated'],
    'food/edit' => ['middleware' => 'authenticated'],
    'food/delete' => ['middleware' => 'authenticated'],
    'inventory' => ['middleware' => 'authenticated'],
    'inventory/add' => ['middleware' => 'authenticated'],
    'inventory/delete' => ['middleware' => 'authenticated'],
    'profile' => ['middleware' => 'authenticated'],
    'profile/password' => ['middleware' => 'authenticated']
];
