<?php
    require_once BASE_PATH . '/setup.php';
    $db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if (mysqli_connect_errno()) {
        echo "Error trying to connect to database";
    }
    mysqli_query($db, "set names 'utf8'");
