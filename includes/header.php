<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="<?=BASE_URL?>/css/styles.css">
    <title><?=APP_NAME?></title>
  </head>
  <body class="<?php body_class(); ?>">
    <!-- Header -->
    <nav id="header" class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="<?=BASE_URL?>">
		        <?=APP_NAME?>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <?php if (session('user')): ?>
                <ul class="navbar-nav bd-navbar-nav flex-row">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="recipe-dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Recipes
                        </a>
                        <div class="dropdown-menu" aria-labelledby="recipe-dropdown">
                            <a class="dropdown-item" href="/recipes/create">Crear</a>
                            <a class="dropdown-item" href="/recipes">Listar</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="recipe-dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Food
                        </a>
                        <div class="dropdown-menu" aria-labelledby="recipe-dropdown">
                            <a class="dropdown-item" href="/food/create">Crear</a>
                            <a class="dropdown-item" href="/food">Listar</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="recipe-dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Inventory
                        </a>
                        <div class="dropdown-menu" aria-labelledby="recipe-dropdown">
                            <a class="dropdown-item" href="/inventory/add">Add food to inventory</a>
                            <a class="dropdown-item" href="/inventory">See inventory</a>
                        </div>
                    </li>
                </ul>
                <?php endif; ?>
                <ul class="navbar-nav ml-auto">
                    <?php if (session('user')): ?>
                    <li class="nav-item">
                        <a href="/profile" class="nav-link">
                            <?=session('user.username');?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= BASE_URL ?>/logout">Log out</a>
                    </li>
                    <?php else: ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=BASE_URL?>/login">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link link-important" href="<?=BASE_URL?>/signup">Sign Up</a>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Header -->
