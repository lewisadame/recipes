<?php

require_once BASE_PATH . '/setup.php';

if (!session('user')) {
    redirect(BASE_URL . '/login');
}
