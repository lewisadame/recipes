<?php
require_once '../setup.php';

if (session('user')) {
    redirect(BASE_URL);
}
