<?php
    require_once '../setup.php';
    require_once '../includes/header.php';
?>
<main class="container">
    <div class="offset-3 col-6 pt-4 pb-4">
        <form action="" method="POST" novalidate>
        <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control <?=($errors['username'])?"is-invalid":""?>" id="username" name="username" aria-describedby="usernameHelp" placeholder="Enter a username" value="<?=($username??'')?>">
                <small id="usernameHelp" class="form-text text-muted">At least 8 characters with lowercase letters and numbers</small>
                <?php errors($errors, 'username'); ?>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control <?=($errors['email'])?"is-invalid":""?>" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter your email"  value="<?=($email??'')?>">
                <small id="emailHelp" class="form-text text-muted">We won't share your email with anyone</small>
                <?php errors($errors, 'email'); ?>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control <?=($errors['password'])?"is-invalid":""?>" id="password" name="password"  placeholder="Password">
                <small id="passwordHelp" class="form-text text-muted">At least 6 characters</small>
                <?php errors($errors, 'password'); ?>
            </div>
            <div class="form-group">
                <label for="password-conf">Confirm password</label>
                <input type="password" class="form-control <?=($errors['passwordconf'])?"is-invalid":""?>" id="password-conf" name="password-conf" placeholder="Enter the password you entered before">
                <?php errors($errors, 'passwordconf'); ?>
            </div>
            <button type="submit" name="signup" class="btn btn-primary">Sign Up</button>
        </form>
    </div>
</main>
<?php require_once '../includes/footer.php'; ?>
