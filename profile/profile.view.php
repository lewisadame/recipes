<?php
require_once '../setup.php';
require_once '../includes/header.php';
?>
<main class="container">
    <?=get_message();?>
    <?php flush_message(); ?>
    <div class="row">
        <?php require_once './includes/sidebar.inc.php'; ?>
        <div class="col-6">
            <form action="" method="POST" novalidate>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" value="<?=session('user.username')?>" class="form-control <?=($errors['username'])?"is-invalid":""?>" id="username" name="username" aria-describedby="usernameHelp" placeholder="Enter a username" value="<?=($username??'')?>">
                    <small id="usernameHelp" class="form-text text-muted">At least 8 characters with lowercase letters and numbers</small>
                    <?php errors($errors, 'username'); ?>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" value="<?=session('user.email')?>" class="form-control <?=($errors['email'])?"is-invalid":""?>" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter your email"  value="<?=($email??'')?>">
                    <small id="emailHelp" class="form-text text-muted">We won't share your email with anyone</small>
                    <?php errors($errors, 'email'); ?>
                </div>
                <button type="submit" name="profile" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
</main>
<?php require_once '../includes/footer.php'; ?>