<?php

require_once '../setup.php';
require_once '../database/connection.php';

if (has_input('profile')) {
    require_once '../validation/Validate.php';

    $rules = [
        'email' => 'required|email',
        'username' => 'required|min:8|regex:/^[a-zA-Z0-9-_]+$/'
    ];

    $data = filter_input_array(INPUT_POST, [
        'username' => FILTER_SANITIZE_STRING,
        'email' => FILTER_SANITIZE_STRING
    ]);

    $validator = new Validate($rules, $data);
    $validator->make();

    if ($validator->fails()) {
        $errors = $validator->messages();
        require_once 'profile.view.php';
    } else {
        // insert this data on the table
        $user_id = session('user.id');
        $query = "update users set username = '{$data['username']}', email = '{$data['email']}' where id = '$user_id' limit 1";

        $result = mysqli_query($db, $query);

        if ($result) {
            message('Your account data was updated!', 'success');

            // update session
            $query = "select * from users where id = '$user_id'";
            $result = mysqli_query($db, $query);
            $data = mysqli_fetch_assoc($result);
            $_SESSION['user'] = $data;

            // redirect
            redirect(BASE_URL . '/profile');
        } else {
            message('We had a problem updating your account data...', 'danger');
            redirect(BASE_URL . '/profile');
        }
    }
} else {
    require_once 'profile.view.php';
}
