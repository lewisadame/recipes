<?php

require_once '../../setup.php';
require_once '../../database/connection.php';

if (has_input('profile-password')) {
    require_once '../../validation/Validate.php';

    $rules = [
        'current-password' => 'required',
        'password' => 'required|min:6|notmatch:current-password',
        'passwordconf' => 'required|match:password'
    ];

    $data = filter_input_array(INPUT_POST, [
        'current-password' => FILTER_SANITIZE_STRING,
        'password' => FILTER_SANITIZE_STRING,
        'passwordconf' => FILTER_SANITIZE_STRING
    ]);

    $validator = new Validate($rules, $data);
    $validator->make();

    if ($validator->fails()) {
        $errors = $validator->messages();
        require_once 'profile-password.view.php';
    } else {

        // check auth
        if (!auth_check($data['current-password'])) {
            $errors['current-password'] = 'The password you provided didn\'t match with your current password';
            require_once 'profile-password.view.php';
            return;
        }

        // insert this data on the table
        $user_id = session('user.id');
        $hashed_password = password_hash($data['password'], PASSWORD_DEFAULT);
        $query = "update users set password = '$hashed_password' where id = '$user_id' limit 1";

        $result = mysqli_query($db, $query);

        if ($result) {
            message('Your password has been updated!', 'success');
            redirect(BASE_URL . '/profile/password');
        } else {
            message('We had a problem updating your password...', 'danger');
            redirect(BASE_URL . '/profile/password');
        }
    }
} else {
    require_once 'profile-password.view.php';
}
