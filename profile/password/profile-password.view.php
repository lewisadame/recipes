<?php
require_once '../../setup.php';
require_once '../../includes/header.php';
?>
    <main class="container">
        <?=get_message();?>
        <?php flush_message(); ?>
        <div class="row">
            <?php require_once '../includes/sidebar.inc.php'; ?>
            <div class="col-6">
                <form action="" method="POST" novalidate>
                    <div class="form-group">
                        <label for="current-password">Your current password</label>
                        <input type="password" class="form-control <?=($errors['current-password'])?"is-invalid":""?>" id="current-password" name="current-password"  placeholder="Your current password">
                        <small id="passwordHelp" class="form-text text-muted">This is to verify that you are entirely sure to do this.</small>
                        <?php errors($errors, 'current-password'); ?>
                    </div>
                    <div class="form-group">
                        <label for="password">New Password</label>
                        <input type="password" class="form-control <?=($errors['password'])?"is-invalid":""?>" id="password" name="password"  placeholder="Password">
                        <small id="passwordHelp" class="form-text text-muted">At least 6 characters and different to your current password.</small>
                        <?php errors($errors, 'password'); ?>
                    </div>
                    <div class="form-group">
                        <label for="password-conf">Confirm you new password</label>
                        <input type="password" class="form-control <?=($errors['passwordconf'])?"is-invalid":""?>" id="password-conf" name="password-conf" placeholder="Enter the password you entered before">
                        <?php errors($errors, 'passwordconf'); ?>
                    </div>
                    <button type="submit" name="profile-password" class="btn btn-primary">Change password</button>
                </form>
            </div>
        </div>
    </main>
<?php require_once '../../includes/footer.php'; ?>