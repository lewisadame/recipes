<?php

require_once '../setup.php';
session_unset();
session_destroy();
redirect(BASE_URL);
