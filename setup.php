<?php

//Display errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);

// general
define('APP_NAME', 'Recipes');
define('BASE_URL', 'http://recipes.test/');
define('BASE_PATH', __DIR__);
define('UPLOADS_PATH', __DIR__ . '/uploads');

// db
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_NAME', 'recipes');

// session
session_start();

// helpers
require_once 'helpers.php';

// middleware logic
function loadMiddleware($file)
{
    require_once BASE_PATH . '/middleware/' . $file . '.php';
}

function watch_middlewares()
{
    require_once 'routes.php';
    $currentURL = substr($_SERVER['REQUEST_URI'], 1);
    if (substr($currentURL, -1) === "/") {
        $currentURL = substr($currentURL, 0, strlen($currentURL) - 1);
    }
    if ($routes[$currentURL]) {
        loadMiddleware($routes[$currentURL]['middleware']);
    }
}

watch_middlewares();
set_body_class([]);
