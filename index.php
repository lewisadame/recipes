<?php
    require_once 'setup.php';
    require_once 'database/connection.php';
    require_once 'includes/header.php';
?>
<main class="container pt-2">
    <?=get_message();?>
    <?php flush_message(); ?>
    <?php if (session('user')): ?>
        <h1>Hello, <?=session('user.username');?></h1>
    <?php else: ?>
        <div class="jumbotron">
            <h1 class="display-4">Welcome!</h1>
            <p class="lead">With recipes you can save recipes, edit them, delete them, and keep them listed for further reading.</p>
            <hr class="my-4">
            <p>However, in the future it will do <u>much</u> more, stay tuned for the development of this app…</p>
            <a class="btn btn-primary btn-lg" href="/recipes/create" role="button">Save your first recipe!</a>
        </div>
    <?php endif; ?>
</main>

<?php require_once 'includes/footer.php'; ?>
