# Recipes

## Description
Recipes is a simple application for managing your recipes and share them with other users.

## Requirements

- PHP
- MySQL or MariaDB

# Setup

1. Install php and mysql
2. Clone the project to your own machine
3. Change database credentials to your own in the following files:
    - in setup.php
    - and in the database_setup.sql (just the db name)
4. Import sql
    ```bash
    mysql -u root
    > source database_setup.sql
    ```
5. Make sure you have the db and server service running.
6. Run `php migrate`

## TODOS
Gestionar como mínimo 5 tablas (usuarios y logins incluidos) ✔
Las otras tres tablas tendrán relaciones y crud ✔
Validar todas las entradas ✔
Registro / Login ✔
Gestion de sesiones ✔
Gestion del perfil ✔
Proteger las url mediante middlewares ✔
