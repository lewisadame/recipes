-- Drop and create db
drop database if exists recipes;
create database recipes;
use recipes;

-- Create tables

-- Users
create table users (
    id int primary key auto_increment,
    username varchar(100) not null unique,
    email varchar(100) not null unique,
    password varchar(255) not null,
    created_at datetime not null default now(),
    updated_at datetime not null default now() on update now()
);

create table logins (
    user_id int not null,
    successful boolean not null,
    from_ip varchar(20) not null,
    happened_at datetime not null default now(),
    constraint fk_logins_users foreign key (user_id) references users(id)
);

create table food_types (
    id int primary key auto_increment,
    value varchar(50) not null,
    created_at datetime not null default now(),
    updated_at datetime not null default now() on update now()
);

create table recipes (
    id int primary key auto_increment,
    user_id int not null,
    name varchar(255) not null,
    cooking_time float not null,
    origin varchar(100) not null,
    type int not null,
    description text not null,
    created_at datetime not null default now(),
    updated_at datetime not null default now() on update now(),
    constraint fk_recipes_user foreign key (user_id) references users(id),
    constraint fk_recipes_type foreign key (type) references food_types(id)
);

create table recipes_images (
    id int primary key auto_increment,
    recipe_id int not null,
    path varchar(255) not null,
    constraint fk_recipes_images foreign key (recipe_id) references recipes(id) on delete cascade
);

create table food (
    id int primary key auto_increment,
    name varchar(100) not null,
    expiration_date datetime not null
);

create table inventories (
    id int primary key auto_increment,
    user_id int not null,
    constraint fk_inventory_user foreign key (user_id) references users(id) on delete cascade
);

create table inventories_food (
    inventory_id int not null,
    food_id int not null,
    constraint foreign key (inventory_id) references inventories(id) on delete cascade,
    constraint foreign key (food_id) references food(id)
);



