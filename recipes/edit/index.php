<?php
require_once '../../setup.php';
require_once '../../database/connection.php';
require_once '../../validation/Validate.php';

if (has_input('edit-recipe')) {
    $user_id = session('user.id');
    $query = "select * from recipes where id='$id' and user_id='$user_id' limit 1";
    $result = mysqli_query($db, $query);
    $recipe = mysqli_fetch_assoc($result);
    if (!$recipe) {
        redirect(BASE_URL . 'recipes');
    }

    $rules = [
        'recipe_id' => 'integer',
        'name' => 'string',
        'time' => 'integer',
        'origin' => 'string',
        'type' => 'integer'
    ];

    $data = filter_input_array(INPUT_POST, [
        'recipe_id' => FILTER_SANITIZE_NUMBER_INT,
        'name' => FILTER_SANITIZE_STRING,
        'time' => FILTER_SANITIZE_NUMBER_FLOAT,
        'origin' => FILTER_SANITIZE_STRING,
        'type' => FILTER_SANITIZE_STRING,
        'description' => FILTER_SANITIZE_STRING
    ]);

    $validator = new Validate($rules, $data);
    $validator->make();

    if ($validator->fails()) {
        $errors = $validator->messages();
        $query = "select id, value from food_types";
        $result = mysqli_query($db, $query);
        $types = mysqli_fetch_all($result, MYSQLI_ASSOC);
        require_once 'edit-recipe.view.php';
    } else {
        $query = "update recipes set name = '{$data['name']}', cooking_time = '{$data['time']}', origin = '{$data['origin']}', type = '{$data['type']}', description = '{$data['description']}' where id = '{$data['recipe_id']}'";

        $result = mysqli_query($db, $query);

        if ($result) {
            message('Your recipe was edited!', 'success');
            redirect(BASE_URL . '/recipes');
        } else {
            message('We had a problem editing your recipe...', 'danger');
            redirect(BASE_URL . '/recipes');
        }
    }
} else {
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

    if ($id) {
        $user_id = session('user.id');
        $query = "select * from recipes where id='$id' and user_id='$user_id' limit 1";
        $result = mysqli_query($db, $query);
        $recipe = mysqli_fetch_assoc($result);
        if ($recipe) {
            $query = "select id, value from food_types";
            $result = mysqli_query($db, $query);
            $types = mysqli_fetch_all($result, MYSQLI_ASSOC);
            require_once 'edit-recipe.view.php';
        } else {
            redirect(BASE_URL . '/recipes');
        }
    } else {
        redirect(BASE_URL . '/recipes');
    }
}
