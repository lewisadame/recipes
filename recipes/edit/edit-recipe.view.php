<?php
require_once '../../includes/header.php';
?>
<main class="container">
    <div class="offset-3 col-6 pt-4 pb-4">
        <h2>Editar recipe</h2>
        <form action="" method="POST" novalidate>
        <input type="hidden" name="recipe_id" value="<?=$recipe['id'];?>">
        <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control <?= ($errors['name']) ? "is-invalid" : "" ?>" id="name" name="name" aria-describedby="nameHelp" placeholder="Enter a recipe name" value="<?= ($recipe['name'] ?? '') ?>">
                <small id="nameHelp" class="form-text text-muted">Write the title or name of your recipe</small>
                <?php errors($errors, 'name'); ?>
            </div>
            <div class="form-group">
                <label for="time">Preparation time</label>
                <input type="number" class="form-control <?= ($errors['time']) ? "is-invalid" : "" ?>" id="time" name="time" aria-describedby="timeHelp" placeholder="Enter the recipe's preparation time" value="<?= ($recipe['cooking_time'] ?? '') ?>">
                <?php errors($errors, 'time'); ?>
            </div>
            <div class="form-group">
                <label for="origin">Recipe's origin</label>
                <input type="origin" class="form-control <?= ($errors['origin']) ? "is-invalid" : "" ?>" id="origin" name="origin"  placeholder="The recipe's origin" value="<?= ($recipe['origin'] ?? '') ?>">
                <small id="originHelp" class="form-text text-muted">Where is this recipe popular from? Is it a spanish popular recipe?</small>
                <?php errors($errors, 'origin'); ?>
            </div>
            <div class="form-group">
                <label for="type">Recipe's type</label>
                <select name="type" id="type" class="form-control <?= ($errors['type']) ? "is-invalid" : "" ?>">
                    <option selected value="">Select the recipe's type</option>
                    <?php foreach ($types as $type) : ?>
                    <option value="<?= $type['id']; ?>"><?= $type['value']; ?></option>
                    <?php endforeach; ?>
                </select>
                <small id="typeHelp" class="form-text text-muted">Is it a dinner? Dessert? Launch?</small>
                <?php errors($errors, 'type'); ?>
            </div>
            <div class="form-group">
                <label for="description">Recipe's description</label>
                <textarea type="description" class="form-control <?= ($errors['description']) ? "is-invalid" : "" ?>" id="description" name="description"  placeholder="The recipe's description"><?= ($recipe['description'] ?? '') ?></textarea>
                <small id="descriptionHelp" class="form-text text-muted">Explain how you prepare your meal here!</small>
                <?php errors($errors, 'description'); ?>
            </div>
            <button type="submit" name="edit-recipe" class="btn btn-primary">Edit recipe!</button>
            <a href="/recipes" class="btn btn-link">Cancelar</a>
        </form>
    </div>
</main>
<?php require_once '../../includes/footer.php'; ?>
