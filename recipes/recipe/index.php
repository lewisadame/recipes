<?php

require_once '../../setup.php';
require_once '../../database/connection.php';
require_once '../../validation/Validate.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

if ($id) {
    $user_id = session('user.id');
    $query = "select r.*, i.path from recipes r left join recipes_images i on r.id = i.recipe_id where r.id='$id' limit 1";
    $result = mysqli_query($db, $query);
    $recipe = mysqli_fetch_assoc($result);
    if ($recipe) {
        require_once 'recipe.view.php';
    } else {
        redirect(BASE_URL . '/recipes');
    }
} else {
    redirect(BASE_URL . '/recipes');
}
