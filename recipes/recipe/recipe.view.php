<?php
require_once '../../setup.php';
require_once '../../includes/header.php';
?>
<main class="container">
    <?= get_message(); ?>
    <?php flush_message(); ?>
    <div class="card recipe">
        <div class="card-body d-flex flex-column">
            <img src="<?=$recipe['path']?>" class="card-img-top w-25 img-fluid rounded">
            <h5 class="card-title">
                <?= $recipe['name']; ?>
            </h5>
            <h6 class="card-subtitle mb-2 text-muted">
                <?= $recipe['origin']; ?> - <?= $recipe['type'] ?> - <?= $recipe['cooking_time']; ?>h
            </h6>
            <p class="card-text flex-grow-1"><?=$recipe['description']?></p>
            <?php if (session('user.id') === $recipe['user_id']): ?>
            <div class="options">
                <a href="/recipes/edit?id=<?= $recipe['id']; ?>" class="btn btn-primary">Editar</a>
                <form action="/recipes/delete" method="post">
                    <input type="hidden" name="recipe_id" value="<?= $recipe['id']; ?>">
                    <button class="btn btn-link" name="delete-recipe">Delete</button>
                </form>
            </div>
            <?php endif; ?>
        </div>
    </div>
</main>
<?php require_once '../../includes/footer.php'; ?>
