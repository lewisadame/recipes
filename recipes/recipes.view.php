<?php
require_once '../setup.php';
require_once '../includes/header.php';
?>
<main class="container">
    <?=get_message();?>
    <?php flush_message(); ?>
    <h1>Recipes</h1>
    <div class="recipes">
        <?php if (count($recipes) > 0): foreach ($recipes as $recipe): ?>
        <div class="card recipe">
            <div class="card-body d-flex flex-column">
                <h5 class="card-title">
                    <a href="/recipes/recipe?id=<?=$recipe['id']?>">
                        <?=$recipe['name'];?>
                    </a>
                </h5>
                <h6 class="card-subtitle mb-2 text-muted">
                    <?=$recipe['origin'];?> - <?=$recipe['type']?> - <?=$recipe['cooking_time'];?>h
                </h6>
                <p class="card-text flex-grow-1"><?=excerpt($recipe['description'], 170);?></p>
                <h6 class="card-subtitle mb-2 text-muted">
                    by <?=$recipe['author']?>
                </h6>
                <?php if (session('user.id') === $recipe['user_id']) : ?>
                <div class="options">
                    <a href="/recipes/edit?id=<?=$recipe['id'];?>" class="btn btn-primary">Editar</a>
                    <form action="/recipes/delete" method="post">
                        <input type="hidden" name="recipe_id" value="<?=$recipe['id'];?>">
                        <button class="btn btn-link" name="delete-recipe">Delete</button>
                    </form>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <?php endforeach; else: ?>
        Oops no hay recetas!
        <?php endif; ?>
    </div>
</main>
<?php require_once '../includes/footer.php'; ?>
