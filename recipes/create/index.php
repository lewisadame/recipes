<?php

require_once '../../setup.php';
require_once '../../validation/Validate.php';
require_once BASE_PATH . '/database/connection.php';

if (has_input('create-recipe')) {
    $rules = [
        'name' => 'required|string',
        'time' => 'required|integer',
        'origin' => 'required|string',
        'type' => 'required|integer',
        'image' => 'image|size:10',
        'description' => 'required'
    ];

    $data = filter_input_array(INPUT_POST, [
        'name' => FILTER_SANITIZE_STRING,
        'time' => FILTER_SANITIZE_NUMBER_FLOAT,
        'origin' => FILTER_SANITIZE_STRING,
        'type' => FILTER_SANITIZE_STRING,
        'description' => FILTER_SANITIZE_STRING
    ]);

    $validator = new Validate($rules, $data);
    $validator->make();

    if ($validator->fails()) {
        $errors = $validator->messages();
        $query = "select id, value from food_types";
        $result = mysqli_query($db, $query);
        $types = mysqli_fetch_all($result);
        require_once 'create-recipe.view.php';
    } else {
        // insert this data on the table

        if (isset($_FILES['image'])) {
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            if (false === $ext = array_search(
                $finfo->file($_FILES['image']['tmp_name']),
                array(
                    'jpg' => 'image/jpeg',
                    'png' => 'image/png',
                    'gif' => 'image/gif',
                ),
                true
            )) {
                $errors['image'] = "El tipo de imágen no es válido.";
                require_once 'create-recipe.view.php';
                return;
            }

            $file_name = sprintf(
                '../../uploads/%s.%s',
                sha1_file($_FILES['image']['tmp_name']),
                $ext
            );

            if (!move_uploaded_file(
                $_FILES['image']['tmp_name'],
                $file_name
            )) {
                message('We had a problem saving your recipe\'s image...', 'danger');
                redirect(BASE_URL . '/recipes');
                return;
            }
        }

        $user_id = session('user.id');
        $query = "INSERT INTO recipes (name, user_id, cooking_time, origin, type, description) VALUES ('{$data['name']}', '{$user_id}', '{$data['time']}', '{$data['origin']}', '{$data['type']}', '{$data['description']}')";

        $result = mysqli_query($db, $query);

        if ($result) {
            if ($file_name) {
                $recipe_id = mysqli_insert_id($db);
                $query = "INSERT INTO recipes_images (recipe_id, path) VALUES ('{$recipe_id}', '{$file_name}')";
                $result = mysqli_query($db, $query);

                if ($result) {
                    message('Your recipe was saved!', 'success');
                    redirect(BASE_URL . '/recipes');
                } else {
                    message('We had a problem saving your recipe...', 'danger');
                    redirect(BASE_URL . '/recipes');
                }
            } else {
                message('Your recipe was saved!', 'success');
                redirect(BASE_URL . '/recipes');
            }
        } else {
            message('We had a problem saving your recipe...', 'danger');
            redirect(BASE_URL . '/recipes');
        }
    }
} else {
    set_body_class([]);
    $query = "select id, value from food_types";
    $result = mysqli_query($db, $query);
    $types = mysqli_fetch_all($result, MYSQLI_ASSOC);
    require_once 'create-recipe.view.php';
}
