<?php
require_once '../../setup.php';
require_once '../../database/connection.php';
require_once '../../validation/Validate.php';

if (has_input('delete-recipe')) {
    $data = filter_input_array(INPUT_POST, [
        'recipe_id' => FILTER_SANITIZE_NUMBER_INT
    ]);

    $user_id = session('user.id');
    $query = "select * from recipes where id='{$data['recipe_id']}' and user_id='$user_id' limit 1";
    $result = mysqli_query($db, $query);
    $recipe = mysqli_fetch_assoc($result);
    if (!$recipe) {
        redirect(BASE_URL . '/recipes');
        return;
    }

    $rules = [
        'recipe_id' => 'integer'
    ];

    $validator = new Validate($rules, $data);
    $validator->make();

    if ($validator->fails()) {
        redirect(BASE_URL . '/recipes');
    } else {
        $query = "delete from recipes where id = '{$data['recipe_id']}' limit 1";

        $result = mysqli_query($db, $query);

        if ($result) {
            message('Your recipe was deleted!', 'success');
            redirect(BASE_URL . '/recipes');
        } else {
            message('We had a problem deleting your recipe...', 'danger');
            redirect(BASE_URL . '/recipes');
        }
    }
} else {
    redirect(BASE_URL . '/recipes');
}
