<?php
require_once '../setup.php';
require_once '../database/connection.php';
$user_id = session('user.id');
$query = "select r.id, r.user_id, r.name, r.origin, ft.value as type, r.cooking_time, r.description, u.username as author from recipes r join food_types ft on r.type = ft.id join users u on r.user_id = u.id";
$result = mysqli_query($db, $query);
$recipes = mysqli_fetch_all($result, MYSQLI_ASSOC);
require_once 'recipes.view.php';
