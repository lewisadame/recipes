<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Formulario de ejemplo</title>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }
        form {
            width: 30%;
        }
        .form-group {
            margin-bottom: 1em;
        }
        .form-group .content {
            display: flex;
            align-items: center;
            justify-content: space-between;
            margin-bottom: 5px;
        }
        .error {
            background: rgba(255, 0,0, 0.5);
            padding: 5px;
        }
    </style>
</head>

<body>
    <h1>Formulario</h1>
    <form action="" method="post" novalidate>
        <div class="form-group">
            <div class="content">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" value="<?=old('name')?>">
            </div>
            <?php if (isset($errors['name'])) : ?>
            <div class="error">
                <?=$errors['name']?>
            </div>
            <?php endif; ?>
        </div>

        <div class="form-group">
            <div class="content">
                <label for="surname">Surname</label>
                <input type="text" id="surname" name="surname" value="<?= old('surname') ?>">
            </div>
            <?php if (isset($errors['surname'])) : ?>
            <div class="error">
                <?= $errors['surname'] ?>
            </div>
            <?php endif; ?>
        </div>

        <div class="form-group">
            <div class="content">
                <label for="age">Age</label>
                <input type="number" id="age" name="age" value="<?= old('age') ?>">
            </div>
            <?php if (isset($errors['age'])) : ?>
            <div class="error">
                <?= $errors['age'] ?>
            </div>
            <?php endif; ?>
        </div>

        <div class="form-group">
            <div class="content">
                <label for="email">Email</label>
                <input type="email" id="email" name="email" value="<?= old('email') ?>">
            </div>
            <?php if (isset($errors['email'])) : ?>
            <div class="error">
                <?= $errors['email'] ?>
            </div>
            <?php endif; ?>
        </div>

        <div class="form-group">
            <div class="content">
                <label for="password">Password</label>
                <input type="password" id="password" name="password">
            </div>
            <?php if (isset($errors['password'])) : ?>
            <div class="error">
                <?= $errors['password'] ?>
            </div>
            <?php endif; ?>
        </div>

        <input type="submit" name="send" value="Send">
    </form>
</body>

</html>
