<?php

require_once 'RuleMethodNotExistsException.php';

class Validate
{
    protected $rules;
    protected $data;
    protected $errors = [];
    protected $messages;

    public function __construct(array $rules, array $data)
    {
        $this->rules = $rules;
        $this->data = $data;
        $this->messages = require_once 'ValidationMessages.php';
    }

    public function make()
    {
        foreach ($this->rules as $field => $rule) {
            $rules = explode('|', $rule);
            foreach ($rules as $rule) {
                if (strpos($rule, ':') !== false) {
                    [$rule, $ruleValue] = explode(':', $rule);
                }
                if (method_exists($this, $rule)) {
                    if (isset($ruleValue)) {
                        if ($this->{$rule}($ruleValue, $this->data[$field])) {
                            $this->errors[$field] = $this->messages[$rule]($field, $ruleValue);
                            break;
                        }
                    } else {
                        if ($this->{$rule}($this->data[$field])) {
                            $this->errors[$field] = $this->messages[$rule]($field);
                            break;
                        }
                    }
                } else {
                    throw new RuleMethodNotExistsException();
                }
            }
        }
    }

    public function fails()
    {
        return !empty($this->errors);
    }

    public function messages()
    {
        return $this->errors;
    }

    private function required($value)
    {
        return empty($value);
    }

    private function string($value)
    {
        return !!preg_match('/[0-9]+/', $value);
    }

    private function integer($value)
    {
        return !!preg_match('/[a-zA-Z]+/', $value);
    }

    private function email($value)
    {
        return !filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    private function regex($pattern, $value)
    {
        return !preg_match($pattern, $value);
    }

    private function min($len, $value)
    {
        return strlen($value) < $len;
    }

    private function match($fieldToMatch, $value)
    {
        return $this->data[$fieldToMatch] === $value;
    }

    private function notmatch($fieldToMatch, $value)
    {
        return $this->data[$fieldToMatch] === $value;
    }

    private function image($value)
    {
        if (!isset($_FILES['image']['error']) ||
            is_array($_FILES['image']['error'])) {
            return true;
        }

        switch ($_FILES['upfile']['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                return true;
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                return true;
            default:
                return true;
        }
    }

    private function size($value)
    {
        $value = $value * 1000 * 1000; // mb -> kb -> b
        return $_FILES['image']['size'] > $value;
    }

    private function date($value)
    {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') == $date;
    }
}
