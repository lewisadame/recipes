<?php

require_once './helpers.php';

if (isset($_POST['send'])) {
    $data = filter_input_array(INPUT_POST, [
        'name' => ['filters' => FILTER_SANITIZE_STRING],
        'surname' => ['filters' => FILTER_SANITIZE_STRING],
        'age' => ['filters' => FILTER_SANITIZE_NUMBER_INT],
        'email' => ['filters' => FILTER_SANITIZE_EMAIL],
        'password' => ['filters' => FILTER_SANITIZE_STRING],
    ]);

    $rules = [
        'name' => 'required|string',
        'surname' => 'required|string',
        'age' => 'required|integer',
        'email' => 'required|email',
        'password' => 'required|string'
    ];

    require_once './Validate.php';

    $validation = new Validate($rules, $data);
    $validation->make();
    if ($validation->fails()) {
        $errors = $validation->messages();
        require_once './views/form.inc.php';
    } else {
        require_once './views/data.inc.php';
    }
} else {
    $errors = [];
    $data = [];
    require_once './views/form.inc.php';
}
