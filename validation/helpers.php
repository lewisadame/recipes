<?php

if (!function_exists('old')) {
    function old($field)
    {
        global $data;
        return $data[$field] ?? '';
    }
}
