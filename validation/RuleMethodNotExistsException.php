<?php

class RuleMethodNotExistsException extends Exception
{
    public function __construct($code = 0, Exception $previous = null)
    {
        $message = 'This rule method does not exists';
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString()
    {
        return __class__ . ": [{$this->code}]: {$this->message}\n";
    }
}
