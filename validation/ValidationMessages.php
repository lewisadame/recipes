<?php
return [
    'required' => function ($field) {
        return "El campo {$field} está vacío y es obligatorio.";
    },
    'string' => function ($field) {
        return "El campo {$field} no puede contener números.";
    },
    'integer' => function ($field) {
        return "El campo {$field} no puede contener letras.";
    },
    'email' => function ($field) {
        return "El campo {$field} tiene que ser un email correcto.";
    },
    'regex' => function ($field) {
        return "El campo {$field} tiene que seguir el patrón indicado.";
    },
    'min' => function ($field, $len) {
        return "El campo {$field} tiene que tener mínimo {$len} caractéres";
    },
    'match' => function ($fieldToMatch, $currentField) {
        return "El campo {$currentField} tiene que coincidir con {$fieldToMatch}";
    },
    'notmatch' => function ($fieldToMatch, $currentField) {
        return "El campo {$currentField} no puede coincidir con {$fieldToMatch}";
    },
    'image' => function ($field) {
        return "El campo {$field} no contiene una imágen";
    },
    'size' => function ($field, $value) {
        return "El campo {$field} debe contener un archivo de tamaño menor que {$value}mb";
    },
    date => function ($field) {
        return "El campo {$field} debe contener una fecha valida";
    }
];
